var kviz = [ ['Koliko je 12 + 13?',1],['Koliko je 5 * 14?',1],['Barva trave je?', 1], ['Kdaj je bila francoska revolucija',1] ];
var ocena = ['Nezadostno','Zadostno','Dobro','Prav Dobro','Odlično'];
var i = 0;
var stevec = 0;
function naprej() {
  $('h3#vprasanje').html(kviz[i][0]);
}
naprej();
var zmage = 0;
if(document.cookie) {
  zmage = getCookie('zmaga');
  $('.stZmag').html(zmage);
}

$('.tipka2').click(
  function(){
    var odgovor = $('#odgovor').val();
    if (stevec == -20) {
      window.location.replace('http://www.janezjansa.si');
    }
    if (odgovor == kviz[i][1]) {
      $('#odgovor').val('');
      $('.alert-danger').hide();
      $('.alert-success').fadeIn();
      stevec++;     
      if (i < kviz.length-1) {
        i++;
        naprej();
      }
      else {
        var koncna = Math.round(stevec / kviz.length * ocena.length) - 1 ;
        $('#ocena').html(ocena[koncna]);
        $('#rezultat').html(stevec + ' od ' + kviz.length);
        $('#zahvala').modal();
        if(document.cookie) {
          zmage = getCookie('zmaga');
          zmage = parseInt(zmage);
        }
        else {
          zmage = 1;
        }
        zmage++;
        setCookie('zmaga',zmage,3);
        
      }
      
    }
    else {
      $('#odgovor').val('');
      $('.alert-success').hide();
      $('.alert-danger').fadeIn();
      stevec--;
      if (i < kviz.length-1) {
        i++;
        naprej();
      }
    }
    $('div.stevec').html(stevec);
    
  }
);
// indeksni 
var seznam = [20,'Tekst',44,50];
var seznam = [ [20,30],[30,40,50,30] ]
//alert(seznam[1][1]);
// asociativni
//var seznam2 = {ime:'Davor',priimek:'Cerpnjak'}
//alert(seznam2.ime);
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
